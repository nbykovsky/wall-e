import org.scalatest.FunSuite
import robot._

import scalaz.{-\/, \/-}

class TestRobot extends FunSuite{

  test("One move Left") {
    val step = oneMove(Terrain(3, 3), L())(_)
    assert(step(North(1, 1)) == \/-(West(1, 1)), "Left from 1 1 N must be 1 1 W")
    assert(step(East(1, 1)) == \/-(North(1, 1)), "Left from 1 1 E must be 1 1 N")
    assert(step(South(1, 1)) == \/-(East(1, 1)), "Left from 1 1 S must be 1 1 E")
    assert(step(West(1, 1)) == \/-(South(1, 1)), "Left from 1 1 W must be 1 1 S")
  }

  test("One move Right") {
    val step = oneMove(Terrain(3, 3), R())(_)
    assert(step(North(1, 1)) == \/-(East(1, 1)), "Right from 1 1 N must be 1 1 E")
    assert(step(East(1, 1)) == \/-(South(1, 1)), "Right from 1 1 E must be 1 1 S")
    assert(step(South(1, 1)) == \/-(West(1, 1)), "Right from 1 1 S must be 1 1 W")
    assert(step(West(1, 1)) == \/-(North(1, 1)), "Right from 1 1 W must be 1 1 N")
  }

  test("One move Forward (Normal)") {
    val step = oneMove(Terrain(3, 3), M())(_)
    assert(step(North(1, 1)) == \/-(North(1, 2)), "Move from 1 1 N must be 1 2 N")
    assert(step(East(1, 1)) == \/-(East(2, 1)), "Move from 1 1 E must be 2 1 E")
    assert(step(South(1, 1)) == \/-(South(1, 0)), "Move from 1 1 S must be 1 0 S")
    assert(step(West(1, 1)) == \/-(West(0, 1)), "Move from 1 1 W must be 0 1 W")
  }

  test("One move Forward (Bounds)") {
    val trn = Terrain(3, 3)
    val step = oneMove(trn, M())(_)
    assert(step(North(1, 3)) == -\/(RobotLeftTerrain(trn, North(1, 3), M())), "Move from 1 3 N must be error")
    assert(step(East(3, 1)) == -\/(RobotLeftTerrain(trn, East(3, 1), M())), "Move from 3 1 E must be error")
    assert(step(South(1, 0)) == -\/(RobotLeftTerrain(trn, South(1, 0), M())), "Move from 1 0 S must be error")
    assert(step(West(0, 1)) == -\/(RobotLeftTerrain(trn, West(0, 1), M())), "Right from 0 1 W must be error")
  }

  test("Validate") {
    val trn = Terrain(3, 3)
    val valid = validate(trn, _ : Position)
    assert(valid(North(0,0)) == \/-(North(0,0)), "0 0 N must be a valid position")
    assert(valid(North(3,3)) == \/-(North(3,3)), "3 3 N must be a valid position")
    assert(valid(North(-1,0)) == -\/(PointOutsideOfTerrain(trn, North(-1,0))), "-1 0 N must be non valid position")
    assert(valid(North(3,4)) == -\/(PointOutsideOfTerrain(trn, North(3,4))), "3 4 N must be non valid position")
  }

  test("Process task (Inside Terrain)") {
    val trn = Terrain(3, 3)
    assert(processTask(trn, \/-(Task(North(0, 0), List(M(), M(), M(), R(), M(), M(), M(), R())))) == \/-(South(3, 3)), "0 0 N => MMMRMMMR must be 3 3 S")
    assert(processTask(trn, \/-(Task(South(3, 3), List(M(), M(), M(), R(), M(), M(), M(), R())))) == \/-(North(0, 0)), "3 3 S => MMMRMMMR must be 0 0 N")
  }

  test("Process task (Escape Terrain)") {
    val trn = Terrain(3, 3)
    assert(processTask(trn, \/-(Task(North(0, 2), List(M(), M(), M())))) == -\/(RobotLeftTerrain(trn, North(0, 3), M())), "0 2 N => MMM must be error")
    assert(processTask(trn, \/-(Task(East(3, 0), List(M(), R(), R(), R(), R(), M())))) == -\/(RobotLeftTerrain(trn, East(3, 0), M())), "3 0 E => MRRRRM must be error")
  }

  test("Parse terrain") {
    assert(parseTerrain(" 3 3 ") == \/-(Terrain(3,3)), "' 3 3 ' must be valid terrain")
    assert(parseTerrain("0 0") == -\/(TerrainNotValid("0 0")), "'0 0' must be non valid terrain")
    assert(parseTerrain("2147483647 1") == -\/(TerrainNotValid("2147483647 1")), "'2147483647 1' must be non valid terrain")
    assert(parseTerrain("2147483646 1") == \/-(Terrain(Int.MaxValue-1,1)), "'2147483646 1' must be valid terrain")
  }

  test("Program") {
    assert(runRobot(Array()) == -\/(FileNameNotEntered()), "Program shouldn't rum without args")
    assert(runRobot(Array("src/test/scala/testCase0")) == -\/(FileNotFound("src/test/scala/testCase0")), "File doesn't exists")
    assert(runRobot(Array("src/test/scala/testCase1")) == \/-(List(\/-(North(1,3)), \/-(East(5, 1)))), "Test Case 1 must be 1 3 N, 5 1 E")
    assert(runRobot(Array("src/test/scala/testCase2")) == \/-(List(-\/(RouteNotValid("MMMMK")))), "Test case 2 must be error")
    assert(runRobot(Array("src/test/scala/testCase3")) == \/-(List(-\/(PointOutsideOfTerrain(Terrain(5, 5), North(0, 6))))), "Test case 3 must be error")
    assert(runRobot(Array("src/test/scala/testCase4")) == -\/(TerrainNotValid("5 5A")), "Test case 4 must be error")
    assert(runRobot(Array("src/test/scala/testCase5")) == \/-(List(\/-(North(0,0)), \/-(South(1, 1)))), "Test case 5 must be 0 0 N, 1 1 S")
    assert(runRobot(Array("src/test/scala/testCase6")) == \/-(List(\/-(North(0,0)))), "Test case 6 must be 0 0 N")

  }

}
