import scala.annotation.tailrec
import scalaz.syntax.monad._
import scalaz._
import scala.io.Source

object robot {

  type Calculation[T] = \/[Error, T]
  type Coord = Int

  case class Terrain(x: Coord, y: Coord) { override def toString: String = s"$x x $y" }
  case class Task(start: Position, route: List[Move])

  sealed trait Position {
    val x: Coord
    val y: Coord
    val label: String
    override def toString: String = s"$x $y $label"
  }
  case class North(x: Coord, y: Coord) extends Position {val label = "N"}
  case class East(x: Coord, y: Coord) extends Position{ val label = "E" }
  case class South(x: Coord, y: Coord) extends Position{ val label = "S" }
  case class West(x: Coord, y: Coord) extends Position{ val label = "W"}

  /**
    * Functions L, R, M transforms the current position in the following way
    * L - turn left
    * R - turn right
    * M - one step forward
    */
  sealed trait Move extends ((Position) => Position) {
    val label: String
    override def toString: String = label
  }
  case class L() extends Move {
    val label = "Turn Left"
    def apply(position: Position): Position = {
      position match {
        case North(x, y) => West(x, y)
        case East(x, y) => North(x, y)
        case South(x, y) => East(x, y)
        case West(x, y) => South(x, y)
      }
    }
  }
  case class R() extends Move {
    val label = "Turn Right"
    def apply(position: Position):Position = {
      position match {
        case North(x, y) => East(x, y)
        case East(x, y) => South(x, y)
        case South(x, y) => West(x, y)
        case West(x, y) => North(x, y)
      }
    }
  }
  case class M() extends Move {
    val label = "Go Forward"
    def apply(position: Position):Position = {
      position match {
        case North(x, y) => North(x, y + 1)
        case East(x, y) => East(x + 1, y)
        case South(x, y) => South(x, y - 1)
        case West(x, y) => West(x - 1, y)
      }
    }
  }

  sealed trait Error {
    val msg: String
    override def toString: String = s"Error: $msg"
  }
  case class TerrainNotValid(str: String) extends Error{
    val msg = s"Terrain is not valid: $str. Skipping all"
  }
  case class StartPointNotValid(str: String) extends Error {
    val msg = s"Start point is not valid: $str. Skipping current route"
  }
  case class RouteNotValid(str: String) extends Error {
    val msg = s"Route is not valid: $str. Skipping current route"
  }
  case class PointOutsideOfTerrain(trn: Terrain, position: Position) extends Error {
    val msg = s"Start point is outside of terrain: $trn, point: $position. Skipping current route"
  }
  case class RobotLeftTerrain(trn: Terrain, position: Position, move: Move) extends Error {
    val msg = s"Robot escaped from terrain. The last position inside terrain was $position, requested action was $move, terrain: $trn. Skipping the rest of route"
  }
  case class FileNotFound(str: String) extends Error{
    val msg = s"File not found: $str"
  }
  case class UnableReadFile(str: String) extends Error{
    val msg = s"Unable read file: $str"
  }
  case class FileNameNotEntered() extends Error {
    val msg = s"First argument of program must be a name of file"
  }

  /**
    *
    * @param string parses the first line of the input file (string in the following format "<int> <int>")
    *               maximum size of terrain is 2147483646 (bounded by MaxValue of int)
    * @return Left(Terrain) if parsing is successful, Right(TerrainNotValid) otherwise
    */
  def parseTerrain(string: String): Calculation[Terrain] = {
    val patternTerrain = "([0-9]+) ([0-9]+)".r
    string.trim() match {
      case patternTerrain(x, y) if x.toInt > 0 && y.toInt > 0 && x.toInt < Int.MaxValue && y.toInt < Int.MaxValue => \/-(Terrain(x.toInt,y.toInt))
      case _ => -\/(TerrainNotValid(string))
    }
  }

  /**
    * Parses the starting point
    * @param trn - terrain which is used for start point validation
    * @param string - string in the following format "<int> <int> <char>"
    * @return Left(Position) if successful, Right(StartPointNotValid or PointOutsideOfTerrain) otherwise
    */
  def parseStart(trn: Terrain, string: String): Calculation[Position] = {
    val patternStart = "([0-9]+) ([0-9]+) ([NSEW])".r
    val positionMap = Map('W'->West, 'E'->East, 'S'->South, 'N'->North)
    string.trim() match {
      case patternStart(x, y, l) => validate(trn, positionMap(l.head)(x.toInt, y.toInt))
      case _ => -\/(StartPointNotValid(string))
    }
  }

  /**
    * Parces the route
    * @param string - string with route (3 chars allowed M, L, R). Empty string is valid input
    * @return Left(List(Move)) if parsing is successful, Right(RouteNotValid) otherwise
    */
  def parseRoute(string: String): Calculation[List[Move]] = {
    val patternRoute = "([LRM]*)".r
    val routeMap = Map('L'->L, 'R'->R, 'M'->M)
    string.trim() match {
      case patternRoute(xs) => \/-(xs.toList map routeMap map (_ ()))
      case _ => -\/(RouteNotValid(string))
    }
  }

  /**
    * Parses all the lines of the input
    * @param lines in the same order which was in input file
    * @return Left(TerrainNotValid) if unable to parse first line
    *         If first line is parsed  then returns Right(List)
    *         where List includes Lefts for non valid routes and Rights for valid routes
    */
  def parseInput(lines: List[String]): Calculation[(Terrain, List[Calculation[Task]])] = {
    @tailrec
    def parseTasks(trn: Terrain, lines: List[String], acc: List[Calculation[Task]]): List[Calculation[Task]] = {
      lines match {
        case start::route::xs => parseTasks(trn, xs, acc :+ (for { s <- parseStart(trn, start); r <- parseRoute(route)} yield Task(s, r)))
        case start::Nil => acc :+ (for { s <- parseStart(trn, start)} yield Task(s, List()))
        case _ => acc
      }
    }
    for { terrain <- parseTerrain(lines.head) } yield (terrain, parseTasks(terrain, lines.tail, List()))
  }

  /**
    * Checks whether points belongs to terrain
    * @param trn - terrain
    * @param position - points to be tested
    * @return Left(PointOutsideOfTerrain) if point doesn't belong to terrain Right(point) otherwise
    */
  def validate(trn: Terrain, position: Position): Calculation[Position] =
    if ( position.x <= trn.x  &&  position.y <= trn.y && position.x >=0 && position.y >= 0 ) \/-(position)
    else -\/(PointOutsideOfTerrain(trn, position))

  /**
    * Calculates the position after one step
    * @param trn - terrain (used for validation)
    * @param move - one of 3 moves
    * @param position - current position
    * @return Left(RobotLeftTerrain) if move brings robot outside of terrain
    *         Right(New Position) otherwise
    */
  def oneMove(trn: Terrain, move: Move)(position: Position): Calculation[Position] =
    validate(trn, move(position)) leftMap (_ => RobotLeftTerrain(trn, position, move))

  /**
    * Calculate the final position of one robot. If robot escapes terrain,
    * it calculates the last valid position within terrain
    * @param trn - terrain
    * @param task - tast which includes starting point and route
    * @return Left(RobotLeftTerrain) if robot escaped terrain, Right(Final position) otherwise
    */
  def processTask(trn: Terrain, task: Calculation[Task]): Calculation[Position] =
    for {
      tsk <- task
      position <- tsk.route.foldLeft(task map (_ start))(_ >>= oneMove(trn, _))
    } yield position

  /**
    * Transforms input text lines to the valid output
    *
    */
  def processAll(lines: List[String]): Calculation[List[Calculation[Position]]] =
    for {
        input <- parseInput(lines)
        (terrain, tasks) = input
    } yield tasks map (processTask(terrain, _))

  /**
    * Reads input file
    * @param filename - path to the file
    * @return Left(FileNotFound or UnableReadFile) if unsuccesfull
    *         Right(Lines) otherwise
    */
  def readFile(filename: String): Calculation[List[String]] = {
    try {
      \/-(Source.fromFile(filename).getLines.toList.map(_.trim))
    } catch {
      case _ : java.io.FileNotFoundException => -\/(FileNotFound(filename))
      case _ : Throwable => -\/(UnableReadFile(filename))
    }
  }

  /**
    * Function transforms command line arguments to the valid output.
    * Separated for unittests
    *
    */
  def runRobot(args: Array[String]): Calculation[List[Calculation[Position]]] =
    for {
      filename <- {args.toList match {case x::_ => \/-(x); case _ => -\/(FileNameNotEntered())}}
      lines <- readFile(filename)
      result <- processAll(lines)
    } yield result


  def main(args: Array[String]):Unit={
    runRobot(args) match {
      case -\/(err) => println(err)
      case \/-(xs) => xs foreach {case -\/(x)  => println(x); case  \/-(x) => println(x)}
    }
  }

}
