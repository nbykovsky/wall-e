## Wall-e
### How to build and Run
1. sbt assembly
2. cd target/scala-2...
3. java -jar wall-e-assembly-1.0.jar /path/to/input/file

### Input file format
##### Row # 1  
sizes of plateau (two integers separated by space). Maximum size of plateau is 2147483646  
##### Row # (2 * N - 1), where N > 0 
the starting point of Nth robot. Two integers and one char (N,S,E,W) separated by spaces
##### Row # (2 * N), where N > 0 
the route of Nth robot. The string of 3 chars (M,L,R)
### Assumptions
1. Starting point should be inside of plateau
2. If route goes outside of terrain, program returns error, which contents the last valid point inside terrain
3. Empty route is a valid input
4. If the last robot in the file has starting point but doesn't have route, it is evaluated as if the route is empty
5. If plateau is not valid (the first row), the execution of program is terminated
6. If plateau is valid but some routes or starting points are not valid, the only valid routes along with corresponding starting points will be evaluated
